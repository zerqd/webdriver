<?php
namespace Gtt\ATM\Common;

class TestsCommon extends AbstractTestsCommon
{

    public function checkLocatorType ($locator) {
        if (preg_match('/\/\//', $locator) == 1) {
            $locatorType = 'xpath';
        } else if (preg_match('/\(.*\)/', $locator) == 1) {
            $locatorType = 'xpath';
        } else {
            $locatorType = 'id';
        }
        return $locatorType;
    }

    //WebDriver wrappers

    public function doJs ($jsCode)
    {
        $this->i->executeScript($jsCode);
    }

    public function type ($locator, $string) {
        self::isPresent($locator);
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                $this->i->findElement(\WebDriverBy::xpath($locator))->sendKeys($string);
                break;
            case 'id':
                $this->i->findElement(\WebDriverBy::id($locator))->sendKeys($string);
                break;
        }

    }

    public function click ($locator) {
        self::isPresent($locator);
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                $this->i->findElement(\WebDriverBy::xpath($locator))->click();
                break;
            case 'id':
                $this->i->findElement(\WebDriverBy::id($locator))->click();
                break;
        }

    }

    public function go ($uri) {
        $i = 3;
        //page loading timeout
        while ($i > 0) {
            try {
                $this->i->get($uri);
                break;
            } catch (\WebDriverCurlException $e) {
                $i-=1;
            }
        }
    }

    public function isVisible ($locator, $timeout = 35) {
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                    $this->i->wait($timeout)->until(\WebDriverExpectedCondition::visibilityOfElementLocated(\WebDriverBy::xpath($locator)));
                break;
            case 'id':
                    $this->i->wait($timeout)->until(\WebDriverExpectedCondition::visibilityOfElementLocated(\WebDriverBy::id($locator)));
                break;
                }
    }


    public function waitForElementNotVisible ($locator, $timeout = 10) {
        $waitTime = $timeout;
                while ($waitTime > 0) {
                    sleep(1);
                    try {
                        $displayed = $this->i->findElement(\WebDriverBy::xpath($locator))->isDisplayed();
                        if (!$displayed) {
                            break;
                        }
                    } catch (\Exception $e) {
                        break;
                    }
                    $waitTime--;
                }
                if ($waitTime == 0) {
                    throw new \Exception('Element with locator ' . $locator . ' is still visible after ' . $timeout . ' seconds');
                }
    }

    public function waitForElement ($locator, $timeout = 10) {
        $waitTime = $timeout;
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                while ($waitTime > 0) {
                    sleep(1);
                    try {
                        $displayed = $this->i->findElement(\WebDriverBy::xpath($locator))->isDisplayed();
                        if ($displayed) {
                            break;
                        }
                    } catch (\Exception $e) {
                        }
                    $waitTime--;
                }
                    if ($waitTime == 0) {
                        throw new \Exception('Element with locator ' . $locator . ' is not visible after ' . $timeout . ' seconds');
                    }
        }
    }


    public function isPresent ($locator, $timeout = 35) {
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                    $this->i->wait($timeout)->until(\WebDriverExpectedCondition::presenceOfElementLocated(\WebDriverBy::xpath($locator)));
                break;
            case 'id':
                    $this->i->wait($timeout)->until(\WebDriverExpectedCondition::presenceOfElementLocated(\WebDriverBy::id($locator)));
                break;
                }
    }

    public function isNotVisible ($locator, $timeout = 5) {
        switch(self::checkLocatorType($locator)) {
            case 'xpath':
                $this->i->wait($timeout)->until(\WebDriverExpectedCondition::invisibilityOfElementLocated(\WebDriverBy::xpath($locator)));
                break;
            case 'id':
                $this->i->wait($timeout)->until(\WebDriverExpectedCondition::invisibilityOfElementLocated(\WebDriverBy::id($locator)));
                break;
        }
    }


    public function getText ($locator)
    {
        self::isPresent($locator);
        switch (self::checkLocatorType($locator)) {
            case 'xpath':
                return $this->i->findElement(\WebDriverBy::xpath($locator))->getText();
            default:
            case 'id':
                return $this->i->findElement(\WebDriverBy::id($locator))->getText();
        }
    }

    public function getAttributeValue ($locator, $attribute = 'value')
    {
        self::isPresent($locator);
        switch (self::checkLocatorType($locator)) {
            case 'xpath':
                return $this->i->findElement(\WebDriverBy::xpath($locator))->getAttribute($attribute);
            default:
            case 'id':
                return $this->i->findElement(\WebDriverBy::id($locator))->getAttribute($attribute);
            }
    }






}

