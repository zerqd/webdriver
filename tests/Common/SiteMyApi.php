<?php
namespace Gtt\ATM\Common;
use Gtt\ATM\Common\Data\Site;
use Gtt\ATM\Common\TestsCommon;

class SiteMyApi extends AbstractTests
{
    protected $i;

    public function setUp() {
        parent::setUp();
        $this->i = new TestsCommon($this->driver);
    }

    public function inputEmailBody ($emailBody)
    {
        $i = $this->i;
        $emailBodyId = $i->getAttributeValue(Site::$textbox_role, 'id');
        $i->doJs('document.getElementById("' . $emailBodyId . '").innerHTML="' . $emailBody . '"');
    }

}