<?php
namespace Gtt\ATM\Common;

abstract class AbstractTests extends \PHPUnit_Framework_TestCase
{
    protected $driver;

    public $seleniumServer = 'http://localhost:4444/wd/hub';
    public $connection_timeout_in_ms  = 60000;
    public $request_timeout_in_ms  = 60000;

    public function setUp() {

            $options = new \ChromeOptions();
            $caps = \DesiredCapabilities::chrome();
            $caps->setCapability(\ChromeOptions::CAPABILITY, $options);
            $this->driver = \RemoteWebDriver::create($this->seleniumServer, $caps, $this->connection_timeout_in_ms, $this->request_timeout_in_ms
            );
            $this->driver->manage()->window()->maximize();
    }

    public function tearDown() {
        $this->driver->quit();
    }
}