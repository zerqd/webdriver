<?php
namespace Gtt\ATM\Common\Data;

class Site {

    public static $login_inpit              = '//input[contains (@type, "email")]';
    public static $loginNextButton          = 'identifierNext';
    public static $paswdNextButton          = 'passwordNext';
    public static $psswd_inpit              = '//input[contains (@type, "password")]';
    public static $createEmail_button       = '//*[@class="aic"]//*[@role="button"]';
    public static $toEmail_textarea         = '//*[@name="to"]';
    public static $subject_input            = '//*[@name="subjectbox"]';
    public static $email_dialog             = '//*[@role="dialog"]';
    public static $textarea                 = '//textarea';
    public static $textbox_role             = '//*[@role="textbox"]';
    public static $button_role              = '//*[@role="button"]';
    public static $emailSendButton          = '//*[contains (@role, "button") and (contains (@data-tooltip, "Enter"))]';
    public static $sentEmailSubject         = '//*[contains (@role, "main")]//*[contains (@class, "bog")]';
    public static $SentEmailMenuLink        = '//*[contains (@href, "sent")]';


}