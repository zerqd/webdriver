<?php
namespace Gtt\ATM\Common;

use \RemoteWebDriver;

abstract class AbstractTestsCommon {

    protected $i;

    public function __construct(RemoteWebDriver $driver)
    {
        $this->i = $driver;
    }
}