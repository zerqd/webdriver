<?php

namespace Gtt\ATM\Common;
use Gtt\ATM\Common\Data\Site;
use Gtt\ATM\Common\Data\SiteUri;
use Gtt\ATM\Common\Data\Users;

class SendEmailTest extends SiteMyApi {

    public function setUp() {
        parent::setUp();
    }

    public $client;

    public static $emailSubject = 'Grandma is coming!';
    public static $emailBody    = 'Take a shelter!';

    /**
     * @test
     * @group mail
     */

    public function sendEmail()
    {
        $i = $this->i;

        $i->go(SiteUri::$urlBase);
        $i->type(Site::$login_inpit, Users::$defaultEmail);
        $i->click(Site::$loginNextButton);
        $i->waitForElement(Site::$psswd_inpit);
        $i->type(Site::$psswd_inpit, Users::$pw);
        $i->click(Site::$paswdNextButton);
        $i->click(Site::$createEmail_button);
        $i->type(Site::$toEmail_textarea, Users::$defaultEmail);
        $i->type(Site::$subject_input, self::$emailSubject);
        SiteMyApi::inputEmailBody(self::$emailBody);
        $i->click(Site::$emailSendButton);
        $i->waitForElementNotVisible(Site::$email_dialog);
        $i->click(Site::$SentEmailMenuLink);
        $sentEmailSubject = $i->getText(Site::$sentEmailSubject);
        assertEquals($sentEmailSubject, self::$emailSubject,
            "Email subject which was sent not equals to expected . 
        Perhaps email wasn't sent or this test is checking wrong string. SentEmailSubject is " . $sentEmailSubject);
    }

}